﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class SensorIndication : BaseEntity
    {
        public int SensorId { get; set; }

        /// <summary>
        /// Температура
        /// </summary>
        public float Temperature { get; set; }
    }
}
