﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
	/// <summary>
	/// Базовая сущность
	/// </summary>
	public class BaseEntity
	{
		public BaseEntity()
		{
			CreationDate = DateTime.Now;
		}

		/// <summary>
		/// Идентификатор
		/// </summary>
		public Guid Id { get; set; }

		/// <summary>
		/// Дата создания
		/// </summary>
		public DateTime CreationDate { get; set; }
	}
}
