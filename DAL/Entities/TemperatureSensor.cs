﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class TemperatureSensor : BaseEntity
    {
        public TemperatureSensor() : base()
        {
        }

        public TemperatureSensor(string address, string name) : this()
        {
            Address = address;
            Name = name;
        }

        /// <summary>
        /// Наименование сенсора
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Адрес сенсора
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Название устройства.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Путь к изображению.
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Дата рождения ребенка.
        /// </summary>
        /// <value>Дата рождения ребенка.</value>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Пол ребенка.
        /// </summary>
        //public Gender Gender { get; set; }

        public override string ToString()
        {
            return string.Format("Name: {0}; Address: {1}", Name, Address);
        }
    }
}
