﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
	/// <summary>
	/// Показания сенсора температуры
	/// </summary>
	public class SensorReadings : BaseEntity
	{
		/// <summary>
		/// МАС-адрес сенсора
		/// </summary>
		public string SensorAddress { get; set; }

		/// <summary>
		/// Температура
		/// </summary>
		public float Temperature { get; set; }

		/// <summary>
		/// Дата и время когда был сделан замер
		/// </summary>
		public DateTime IndicationsDate { get; set; }
	}
}
