﻿using System;
using System.Linq;
using DAL.Entities;
using FluentData;

namespace DAL
{
	public class RepositorySensorReadings
	{
/*#if DEBUG
		private const string _dbConnectionString = "Server=localhost;Database=thermometer_test;Trusted_Connection=false;User Id=sql_user;Password=rrwt85GJHSr474gf;";
#else
		private const string _dbConnectionString = "Server=tcp:thermometers.database.windows.net,1433;Initial Catalog=thermometer_test;Persist Security Info=False;User ID=brain;Password=uy!3w4jn$;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

#endif*/
		private const string _dbConnectionString = "Server=tcp:thermometers.database.windows.net,1433;Initial Catalog=thermometer_test;Persist Security Info=False;User ID=brain;Password=uy!3w4jn$;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

		private static IDbContext _context;

		static RepositorySensorReadings()
		{
			_context = Context();
		}

		static IDbContext Context()
		{
			return new DbContext().ConnectionString(_dbConnectionString, new SqlServerProvider());
		}

		/// <summary>
		/// Добавить запись
		/// </summary>
		public bool Insert(SensorReadings entity)
		{
			if(entity != null)
			{
				var addItems = _context.Insert<SensorReadings>("SensorReadings", entity)
					.AutoMap(x => x.Id)
					.Execute();
				//.ExecuteReturnLastId<Guid>();

				return addItems > 0;
			}

			return false;
		}

		/// <summary>
		/// Обновить запись
		/// </summary>
		public bool Update(SensorReadings entity)
		{
			int rowsAffected = _context
				.Update<SensorReadings>("SensorReadings", entity)
				.AutoMap(x => x.Id)
				.Where(x => x.Id)
				.Execute();

			return rowsAffected > 0;
		}

		/// <summary>
		/// Удалить запись
		/// </summary>
		public bool Delete(SensorReadings entity)
		{
			int rowsAffected = _context
				.Delete<SensorReadings>("SensorReadings", entity)
				.Where("Id", entity.Id)
				.Execute();

			return rowsAffected > 0;
		}
	}
}
