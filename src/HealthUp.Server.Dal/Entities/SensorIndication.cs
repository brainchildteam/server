﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthUp.Server.Dal.Entities
{
    public class SensorIndication : Base.BaseEntity
    {
        /// <summary>
        /// Температура
        /// </summary>
        public float Temperature { get; set; }
        
        /// <summary>
        /// Термометр
        /// </summary>
        public virtual TemperatureSensor Sensor { get; set; }
    }
}
