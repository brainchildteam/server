﻿using System;

namespace HealthUp.Server.Dal.Entities.Base
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            DateCreated = DateTime.UtcNow;
        }

        public virtual int Id { get; set; }

        public virtual DateTime DateCreated { get; set; }
    }
}
