﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthUp.Server.Dal.Entities.Enums
{
    /// <summary>
    /// Пол
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// Мужчина
        /// </summary>
        Male = 0,

        /// <summary>
        /// Женщина
        /// </summary>
        Female = 1,

        /// <summary>
        /// Неопределившиеся
        /// </summary>
        TransGender = 2
    }
}
