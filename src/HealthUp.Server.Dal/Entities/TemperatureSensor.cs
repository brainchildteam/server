﻿using System;
using System.Collections.Generic;

using HealthUp.Server.Dal.Entities.Enums;

namespace HealthUp.Server.Dal.Entities
{
    public class TemperatureSensor : Base.BaseEntity
    {
        /// <summary>
        /// Наименование сенсора
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Адрес сенсора
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Название устройства.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Путь к изображению.
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Дата рождения ребенка.
        /// </summary>
        /// <value>Дата рождения ребенка.</value>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Пол ребенка.
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Серийный номер
        /// </summary>
        public virtual SerialNumber SerialNumber { get; set; }

        /// <summary>
        /// Показания температуры
        /// </summary>
        public ICollection<SensorIndication> Temparatures { get; set; }
    }
}
