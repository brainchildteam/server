﻿using Autofac;

using HealthUp.Server.Dal.Services;

namespace HealthUp.Server.Dal
{
    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            this.RegisterInstance(builder);
            builder
                .RegisterGeneric(typeof(Services.Impl.Repository<>))
                .As(typeof(IRepository<>))
                .SingleInstance();
        }

        /// <summary>
        /// Регистрация реализаций для зависимостей
        /// </summary>
        private void RegisterInstance(ContainerBuilder builder)
        {
            builder.RegisterInstance(new HealthUpContext()).As(typeof(HealthUpContext)).SingleInstance();
        }
    }
}
