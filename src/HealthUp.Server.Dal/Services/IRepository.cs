﻿using System;
using System.Linq;

namespace HealthUp.Server.Dal.Services
{
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Запрос получения всех записей таблицы
        /// </summary>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Получить запись таблицы по id
        /// </summary>
        TEntity Get(int id);

        /// <summary>
        /// Сохранить объект
        /// </summary>
        bool Create(TEntity item);

        /// <summary>
        /// Обновить объект
        /// </summary>
        bool Update(TEntity item);

        /// <summary>
        /// Удалить конкретную запись
        /// </summary>
        bool Delete(int id);

        /// <summary>
        /// Удалить конкретную запись
        /// </summary>
        bool Delete(TEntity item);
    }
}
