﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;

using HealthUp.Server.Dal.Entities;

namespace HealthUp.Server.Dal.Services
{
    public class HealthUpContext : DbContext
    {
        private const string _dbConnectionString = "Server=tcp:thermometers.database.windows.net,1433;Initial Catalog=thermometer_test;Persist Security Info=False;User ID=brain;Password=uy!3w4jn$;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        //private const string _dbConnectionString = @"data source=.\SQLEXPRESS;Database=thermometer_test;Integrated Security=SSPI";

        public HealthUpContext() : base(_dbConnectionString)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<HealthUpContext, HealthUp.Server.Dal.Migrations.Configuration>());
        }

        public DbSet<SerialNumber> SerialNumber { get; set; }

        public DbSet<SensorIndication> SensorIndication { get; set; }

        public DbSet<TemperatureSensor> TemperatureSensor { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Configure default schema
        }
    }
}
