﻿using System;
using System.Linq;
using System.Data.Entity;

using Autofac;

using HealthUp.Server.Dal.Entities.Base;

namespace HealthUp.Server.Dal.Services.Impl
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private object objLock;
        private HealthUpContext _context;
        private DbSet<TEntity> _dbSet;

        public Repository(IComponentContext container)
        {
            _context = container.Resolve<HealthUpContext>();
            if(_context != null)
            {
                _dbSet = _context.Set<TEntity>();
            }

            objLock = new object();
        }

        public TEntity Get(int id)
        {
            if(_dbSet == null)
            {
                return null;
            }

            return _dbSet.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<TEntity> GetAll()
        {
            if(_dbSet == null)
            {
                return null;
            }

            return _dbSet.AsQueryable();
        }

        public bool Create(TEntity item)
        {
            if(_dbSet == null)
            {
                return false;
            }

            lock(objLock)
            {
                if((item != null))
                {
                    _dbSet.Add(item);
                    return _context.SaveChanges() > 0;
                }

                return false;
            }
        }

        public bool Update(TEntity item)
        {
            if(_dbSet == null)
            {
                return false;
            }

            lock(objLock)
            {
                if(item != null)
                {
                    _context.Entry(item).State = EntityState.Modified;
                    return _context.SaveChanges() > 0;
                }
            }

            return false;
        }

        public bool Delete(int id)
        {
            if(_dbSet == null)
            {
                return false;
            }

            if(id > 0)
            {
                var item = this.Get(id);
                if(item != null)
                {
                    return this.Delete(item);
                }
            }

            return false;
        }

        public bool Delete(TEntity item)
        {
            if(_dbSet == null)
            {
                return false;
            }

            if(item != null)
            {
                _dbSet.Remove(item);
                return _context.SaveChanges() > 0;
            }

            return false;
        }
    }
}
