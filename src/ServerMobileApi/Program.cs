﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using DAL;

namespace ServerMobileApi
{
	public class Program
	{
		private static RepositorySensorReadings _rep;
		public static void Main(string[] args)
		{
			_rep = new RepositorySensorReadings();
			var host = new WebHostBuilder()
				.UseKestrel()
				.UseContentRoot(Directory.GetCurrentDirectory())
				.UseIISIntegration()
				.UseStartup<Startup>()
				.Build();

			host.Run();
		}

		/*public static RepositorySensorReadings Repository
		{
			get
			{
				if(_rep == null)
				{
					_rep = new RepositorySensorReadings();
				}

				return _rep;
			}
		}*/
	}
}
