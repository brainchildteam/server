﻿using System;
using Microsoft.AspNetCore.Mvc;

using Autofac;
using Newtonsoft.Json;

using DAL;
using DAL.Entities;
using ServerMobileApi.Controllers.Base;
using HealthUp.Server.Models;
using HealthUp.Server.Services;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ServerMobileApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class SensorController : BaseController
    {
        private IThermometerService _thermometerService;
        private ISerialNumberService _serialNumberService;

        public SensorController() : base()
        {
            _serialNumberService = _container.Resolve<ISerialNumberService>();
            _thermometerService = _container.Resolve<IThermometerService>();
        }

        [HttpPost]
		public bool AddTemperature([FromBody]SensorReadings data)
		{
            /*var rep = Program.Repository;
			SensorReadings dataSensor = new SensorReadings();
			dataSensor.SensorAddress = data.SensorAddress;
			dataSensor.Temperature = data.Temperature;
			dataSensor.IndicationsDate = data.IndicationsDate;

			if(rep.Insert(dataSensor))
			{
				return true;
			}
			else
			{
				return false;
			}*/
            return false;
		}

        public string Get([FromBody] string serialnumber, string mac)
        {
            TemperatureSensorModel sensor = _thermometerService.GetThermomet(serialnumber, mac);

            if(sensor != null)
            {
                return JsonConvert.SerializeObject(sensor);
            }
            else
            {
                return "Error. Error retrieving data thermometer.";
            }
        }

        /// <summary>
        /// Проверка правильности серийного номера
        /// </summary>
        /// <param name="sensorModel"></param>
        [HttpPost]
        public string ValidateSensor([FromBody] string serialnumber, string macaddress)
        {
            if(string.IsNullOrEmpty(serialnumber))
            {
                return "error: serialnumber is null";
            }

            ResultOfValidation rv = _serialNumberService.Validate(serialnumber, macaddress);
            return rv.Message;
        }

        /// <summary>
        /// Проверка правильности серийного номера
        /// </summary>
        /// <param name="sensorModel"></param>
        [HttpGet]
        public string ValidateSensor()
        {
            var serialnumber = "32196413216";
            var macaddress = "20:C3:8F:P8:2D:9B";
            ResultOfValidation rv = _serialNumberService.Validate(serialnumber, macaddress);
            return rv.Message;
        }

        /// <summary>
        /// Добавить данные о новом устройстве
        /// </summary>
        [HttpPost]
        public string AddSensor([FromBody] TemperatureSensorModel sensorModel)
        {
            if(sensorModel != null)
            {
                ResultOfValidation resultCreation = _thermometerService.CreateSensor(sensorModel);

                return resultCreation.Message;
            }
            
            return "Error. Model is null";
        }
    }
}
