﻿using System;
using Microsoft.AspNetCore.Mvc;

using Autofac;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ServerMobileApi.Controllers.Base
{
    [Route("api/[controller]/[action]")]
    public class BaseController : Controller
    {
        protected IContainer _container;

        public BaseController() : base()
        {
            _container = Startup.Container;
        }
    }
}
