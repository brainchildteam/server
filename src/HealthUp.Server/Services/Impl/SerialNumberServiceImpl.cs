﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autofac;

using HealthUp.Server.Models;
using HealthUp.Server.Dal.Entities;
using HealthUp.Server.Dal.Services;
using Newtonsoft.Json;

namespace HealthUp.Server.Services.Impl
{
    public class SerialNumberServiceImpl : ISerialNumberService
    {
        private IComponentContext _container;

        public SerialNumberServiceImpl(IComponentContext componetContext)
        {
            this._container = componetContext;
        }

        public ResultOfValidation Validate(string serialNumber, string macAddress)
        {
            ResultOfValidation result = new ResultOfValidation()
            {
                Result = true,
                Message = string.Empty
            };

            if(string.IsNullOrEmpty(serialNumber))
            {
                return new ResultOfValidation()
                {
                    Result = false,
                    Message = "Serial number is null"
                };
            }

            IRepository<SerialNumber> repSerialNumber = _container.Resolve<IRepository<SerialNumber>>();
            SerialNumber sn = repSerialNumber.GetAll().Where(x => x.Number.ToLower() == serialNumber.ToLower()).FirstOrDefault();
            if(sn == null)
            {
                return new ResultOfValidation()
                {
                    Result = false,
                    Message = "Serial number is not correct"
                };
            }

            //TODO Если серийник есть, то нужна проверка наличия датчика с таким мак-адресом. Если дачтика нет создать данную пару и в последующем одновить данные по датчику из специального запроса
            IRepository<TemperatureSensor> repTemperatureSensor = _container.Resolve<IRepository<TemperatureSensor>>();
            var querySensor = repTemperatureSensor.GetAll().Where(x => x.SerialNumber.Number.ToLower() == serialNumber);
            if(string.IsNullOrEmpty(macAddress))
            {
                TemperatureSensor sensor = querySensor.FirstOrDefault();
                if(sensor != null)
                {
                    return new ResultOfValidation()
                    {
                        Result = true,
                        Message = JsonConvert.SerializeObject(sensor)
                    };
                }
                else
                {
                    return new ResultOfValidation()
                    {
                        Result = false,
                        Message = "The serial number was not found thermometer"
                    };
                }
            }
            else
            {
                TemperatureSensor sensor = querySensor.Where(x => x.Address.ToLower() == macAddress.ToLower()).FirstOrDefault();
                if(sensor != null)
                {
                    return new ResultOfValidation()
                    {
                        Result = true,
                        Message = sensor.Address
                    };
                }
                else
                {
                    repTemperatureSensor.Create(new TemperatureSensor { SerialNumber = sn, Address = macAddress });
                    return new ResultOfValidation()
                    {
                        Result = true,
                        Message = string.Empty
                    };
                }
            }
        }
    }
}
