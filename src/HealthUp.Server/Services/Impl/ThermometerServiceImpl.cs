﻿using System;
using System.Linq;

using Autofac;

using HealthUp.Server.Models;
using HealthUp.Server.Dal.Entities;
using HealthUp.Server.Dal.Entities.Enums;
using HealthUp.Server.Dal.Services;
using HealthUp.Server.Models.Enums;

namespace HealthUp.Server.Services.Impl
{
    public class ThermometerServiceImpl : IThermometerService
    {
        private IComponentContext _container;

        public ThermometerServiceImpl(IComponentContext componetContext)
        {
            this._container = componetContext;
        }

        public ResultOfValidation CreateSensor(TemperatureSensorModel model)
        {
            if(model == null)
            {
                return new ResultOfValidation()
                {
                    Result = false,
                    Message = "Sensor model is null"
                };
            }

            TemperatureSensor sensorForCreate = new TemperatureSensor();
            sensorForCreate.Address = model.Address;
            sensorForCreate.BirthDate = model.BirthDate;
            sensorForCreate.Image = model.Image;
            sensorForCreate.Name = model.Name;
            sensorForCreate.Title = model.Title;

            if(!string.IsNullOrEmpty(model.SerialNumber))
            {
                IRepository<SerialNumber> repSerialNumber = _container.Resolve<IRepository<SerialNumber>>();
                SerialNumber sn = repSerialNumber.GetAll().Where(x => x.Number.ToLower() == model.SerialNumber.ToLower()).FirstOrDefault();
                if(sn != null)
                {
                    sensorForCreate.SerialNumber = sn;
                }
            }

            if(model.Gender > 0)
            {
                sensorForCreate.Gender = (Gender)model.Gender;
            }

            try
            {
                IRepository<TemperatureSensor> repTemperatureSensor = _container.Resolve<IRepository<TemperatureSensor>>();
                repTemperatureSensor.Create(sensorForCreate);
            }
            catch (Exception e)
            {
                return new ResultOfValidation
                {
                    Result = false,
                    Message = e.Message + "\r\n" + e.StackTrace
                };
            }

            return new ResultOfValidation()
            {
                Result = true,
                Message = string.Empty

            };
        }

        public TemperatureSensorModel GetThermomet(string serialNumber, string macAddress)
        {
            if(string.IsNullOrEmpty(serialNumber))
            {
                return null;
            }

            if(string.IsNullOrEmpty(macAddress))
            {
                return null;
            }

            string serialNumberLower = serialNumber.ToLower();
            string macAddressLower = macAddress.ToLower();

            IRepository<TemperatureSensor> repTemperatureSensor = _container.Resolve<IRepository<TemperatureSensor>>();

            TemperatureSensor sensor = repTemperatureSensor
                                        .GetAll()
                                        .Where(x => x.SerialNumber.Number.ToLower() == serialNumberLower || x.Address.ToLower() == macAddressLower)
                                        .FirstOrDefault();
            if(sensor== null)
            {
                return null;
            }

            TemperatureSensorModel result = new TemperatureSensorModel();
            result.Address = sensor.Address;
            result.BirthDate = sensor.BirthDate;
            result.Image = sensor.Image;
            result.Name = sensor.Name;
            result.Title = sensor.Title;
            result.SerialNumber = sensor.SerialNumber.Number;


            if(sensor.Gender > 0)
            {
                result.Gender = (GenderModel)sensor.Gender;
            }

            return result;
        }
    }
}
