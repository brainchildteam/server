﻿using HealthUp.Server.Models;

namespace HealthUp.Server.Services
{
    public interface ISerialNumberService
    {
        /// <summary>
        /// Проверка валидности серийного номера
        /// </summary>
        ResultOfValidation Validate(string serialNumber, string macAddress);
    }
}
