﻿using System;

using HealthUp.Server.Models;

namespace HealthUp.Server.Services
{
    /// <summary>
    /// Сервис 
    /// </summary>
    public interface IThermometerService
    {
        /// <summary>
        /// Создать запись термометра
        /// </summary>
        ResultOfValidation CreateSensor(TemperatureSensorModel sensorModel);

        /// <summary>
        /// Получить данные термометра, по серийному номеру и мак-адресу
        /// </summary>
        TemperatureSensorModel GetThermomet(string serialNumber, string macAddress);
    }
}
