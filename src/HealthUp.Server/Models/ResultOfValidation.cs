﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthUp.Server.Models
{
    public class ResultOfValidation
    {
        public bool Result;

        public string Message;
    }
}
