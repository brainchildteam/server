﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthUp.Server.Models.Enums
{
    public enum GenderModel
    {
        /// <summary>
        /// Мужчина
        /// </summary>
        Male = 0,

        /// <summary>
        /// Женщина
        /// </summary>
        Female = 1,

        /// <summary>
        /// Неопределившиеся
        /// </summary>
        TransGender = 2
    }
}
