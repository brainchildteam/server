﻿using System;

using HealthUp.Server.Models.Enums;

namespace HealthUp.Server.Models
{
    public class TemperatureSensorModel
    {
        /// <summary>
		/// Наименование сенсора
		/// </summary>
		public string Name { get; set; }

        /// <summary>
        /// Адрес сенсора
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Название устройства.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Путь к изображению.
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Дата рождения ребенка.
        /// </summary>
        /// <value>Дата рождения ребенка.</value>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Пол ребенка.
        /// </summary>
        public GenderModel Gender { get; set; }

        /// <summary>
        /// Серийный номер
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// Флаг, активено ли устройство сейчас или нет
        /// </summary>
        public bool IsActive { get; set; }
    }
}
