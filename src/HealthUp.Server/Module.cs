﻿using System;
using System.Reflection;
using System.Collections.Generic;

using Autofac;

using HealthUp.Server.Services;
using HealthUp.Server.Services.Impl;
using HealthUp.Server.Dal.Services;

namespace HealthUp.Server
{
    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterAssemblies(builder);

            builder
                .RegisterType<SerialNumberServiceImpl>()
                .As<ISerialNumberService>();

            builder
                .RegisterType<ThermometerServiceImpl>()
                .As<IThermometerService>();
        }

        private void RegisterAssemblies(ContainerBuilder builder)
        {
            List<Assembly> asseblies = new List<Assembly>();
            asseblies.Add(typeof(IRepository<>).GetTypeInfo().Assembly);

            foreach(var item in asseblies)
            {
                builder.RegisterAssemblyModules(item);
            }
        }
    }
}
